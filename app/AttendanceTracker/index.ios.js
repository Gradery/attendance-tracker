import React, { Component } from 'react';
import {
  AppRegistry,
  AsyncStorage,
  Dimensions,
  Navigator,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  TouchableElement,
  TouchableHighlight,
  View
} from 'react-native';
import Tracker from './components/tracker';
import Scanner from './components/scanner';
import Login from './components/login';
//import Camera from 'react-native-camera';

var AttendanceTracker = React.createClass({

  renderScene(route, navigator){
    if (route.name == 'Tracker'){
      return(
        <Tracker navigator={navigator} badge_id={route.badge_id} />
      )
    }
    else if (route.name == 'Scanner'){
      return(
        <Scanner navigator={navigator} />
      )
    }
    else
    {
      return(
        <Login navigator={navigator} />
      )
    }
  },

  render() {
    try {
      var token = AsyncStorage.getItem('@Gradery:token').done();
      var initialPage = "";
      if (token !== null) //already logged in
        initialPage = "Tracker"
      else
        initialPage = "Login"
    }
    catch(error){
      console.error(error);
    }
    return(
      <Navigator
        initialRoute={{ name:initialPage }}
        renderScene={ this.renderScene }
      />
    )
  }
});

const styles = StyleSheet.create({
  loginContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    paddingLeft: 10,
    paddingRight: 10,
  },
  trackerContainer:{
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    paddingLeft: 10,
    paddingRight: 10,
  },
  title:{
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 50
  },
  error: {
    height: 15,
    color: 'red',
  },
  button:{
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    marginTop: 5,
    marginBottom: 5,
    paddingRight: 20,
    paddingLeft: 20,
    paddingTop: 15,
    paddingBottom: 15,
    backgroundColor: '#33AAFF',
  },
  scanButton: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    height: 40,
    backgroundColor: '#33AAFF',
    paddingRight: 5,
    paddingLeft: 5,
  },
  textInput: {
    height: 40,
    borderStyle: 'solid',
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 5,
    marginTop: 5,
    marginBottom: 5,
    paddingRight: 10,
    paddingLeft: 10,
  },
  textInputWithButton: {
    height: 40,
    borderStyle: 'solid',
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 5,
    marginTop: 5,
    marginBottom: 5,
    marginRight: 10,
    paddingRight: 10,
    paddingLeft: 10,
    width: 250
  }
});

AppRegistry.registerComponent('AttendanceTracker', () => AttendanceTracker);
