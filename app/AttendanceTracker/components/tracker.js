import React, { Component } from 'react';
import {
  ActivityIndicator,
  Alert,
  AppRegistry,
  AsyncStorage,
  Dimensions,
  Navigator,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  TouchableElement,
  TouchableHighlight,
  View
} from 'react-native';

import Autosuggest from 'react-autosuggest';

export default Tracker = React.createClass({
	componentDidMount() {
	    this._loadInitialState().done();
	    if (this.props.badge_id !== undefined)
			this.setState({loading: false})
	},
	  
	async _loadInitialState() {
	    try {
	      var self = this;
	      var token = await AsyncStorage.getItem('@Gradery:token');
	      if (token !== null)
	        this.setState({token: token});
	    	setTimeout(function(){
	      		self._getStudents().done();
	      	}, 1000);
	    	if (this.props.badge_id !== undefined)
	    	{
	    		return this._checkBadgeId();
	    	}
	      else
	        this.setState({token: null});
	    }
	    catch(error){
	      console.error(error);
	    }
	},

	async _getStudents(){
		var self = this;
		return fetch("http://localhost:3000/members?token="+this.state.token)
		.then((response) => response.json())
		.then((responseJson) => {
			if (responseJson.errors === undefined)
			{
				self.setState({members: responseJson.members});
			}
			else{
				if (responseJson.response.code === 401){
					console.log("Need to log back in");
				}
			}
		})
		.catch((error) => {
	      console.error(error);
	    });
	},

	async _checkBadgeId(){
		if (this.props.badge_id !== undefined)
			this.setState({loading: true})
		return fetch("http://localhost:3000/members/search?badge_id="+this.props.badge_id, {
			headers: {
		        'Content-Type': 'application/json',
		        'Authorization': this.state.token
		    },
		})
		.then((response) => response.json())
	    .then((responseJson) => {
	    	console.log(responseJson);
	      if (responseJson.member !== undefined) //got data
	      {
	        this.setState({
	        	selectedMemberText: responseJson.member.name,
	        	selectedMember: responseJson.member,
	        	selectedMemberId: responseJson.member.id,
	        	error: ''
	        });
			this.setState({loading: false})
	      }
	      else
	        this.setState({error: "No Student Found"});
	    })
	    .catch((error) => {
	      console.error(error);
	    });
	},

	scan(){
		this.props.navigator.push({name: "Scanner"})
  	},

  	markTardy(){
  		this._markTardy().done();
  	},

  	async _markTardy(){
  		var self = this;
  		return fetch("http://localhost:3000/members/"+this.state.selectedMemberId+"/tardy",{
  			token: this.state.token
  		})
  		.then((response) => response.json())
  		.then((responseJson) => {
  			Alert.alert(
			  'Success',
			  'Student Marked Tardy',
			  [
			    {text: 'OK', onPress: () => console.log('OK Pressed')},
			  ]
			)
			self.clearSearch();
  		})
  		.catch((error) => {
	      console.error(error);
	    });
  	},

	getInitialState() {
	    return {
	      token: null,
	      error: '',
	      selectedMemberText: '',
	      selectedMemberId: 0,
	      badge_id: 0,
	      loading: false
	    };
	},
	clearSearch(){
		this.setState({
			selectedMemberText: "",
			selectedMemberId: 0,
			selectedMemberObj: undefined,
			selectedMember: undefined
		});
	},
	render(){
		return(
			<View style={styles.loginContainer}>
			  <ActivityIndicator
        		animating={this.state.loading}
        		size="large"
      		  />
			  <Text style={styles.error}>{this.state.error}</Text>
	          <View style={styles.trackerContainer}>
	            <TextInput 
	              placeholder="Student Name or Badge ID"
	              style={styles.textInputWithButton}
	              autoCorrect={true}
	              autoFocus={true}
	              value={this.state.selectedMemberText}
	              onChangeText={(text) => this.setState({selectedMemberText: text})}
	            />
	            <TouchableHighlight
	            style={styles.scanButton}
	            onPress={this.scan}>
	              <View>
	                <Text>Scan</Text>
	              </View>
	            </TouchableHighlight>
	          </View>
	          {(() =>{
	          	if (this.state.selectedMember !== undefined){
	          		return(
	          			<View>
				            <TouchableHighlight
				            style={styles.tardyButton}
				            onPress={this.markTardy}>
				              <View>
				                <Text style={{color: "white"}}>Mark Tardy</Text>
				              </View>
				            </TouchableHighlight>
				            <TouchableHighlight
				            style={styles.tardyButton}
				            onPress={this.clearSearch}>
				              <View>
				                <Text style={{color: "white"}}>Clear</Text>
				              </View>
				            </TouchableHighlight>
				        </View>
	          		)
	          	}
	          	else {
	          		return(
	          			<View style={{height: 245}}/>
	          		)
	          	}
	          })()}
	        </View>
		);
	}
});

const styles = StyleSheet.create({
  loginContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    paddingLeft: 10,
    paddingRight: 10,
  },
  trackerContainer:{
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    paddingLeft: 10,
    paddingRight: 10,
  },
  title:{
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 50
  },
  error: {
    height: 15,
    color: 'red',
  },
  button:{
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    marginTop: 5,
    marginBottom: 5,
    paddingRight: 20,
    paddingLeft: 20,
    paddingTop: 15,
    paddingBottom: 15,
    backgroundColor: '#33AAFF',
  },
  scanButton: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    height: 40,
    backgroundColor: '#33AAFF',
    paddingRight: 5,
    paddingLeft: 5,
  },
  tardyButton:{
  	justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    height: 80,
    backgroundColor: 'red',
    paddingRight: 5,
    paddingLeft: 5,
    marginTop: 100
  },
  textInput: {
    height: 40,
    borderStyle: 'solid',
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 5,
    marginTop: 5,
    marginBottom: 5,
    paddingRight: 10,
    paddingLeft: 10,
  },
  textInputWithButton: {
    height: 40,
    borderStyle: 'solid',
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 5,
    marginTop: 5,
    marginBottom: 5,
    marginRight: 10,
    paddingRight: 10,
    paddingLeft: 10,
    width: 250
  }
});