import React, { Component } from 'react';
import {
  AppRegistry,
  AsyncStorage,
  Dimensions,
  Navigator,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  TouchableElement,
  TouchableHighlight,
  View
} from 'react-native';

export default Login = React.createClass({
  authenticate: function() {
    this.checkLogin().done();
  },

  checkLogin() {
    return fetch('http://localhost:3000/auth/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: this.state.email,
        password: this.state.password
      })
    })
    .then((response) => response.json())
    .then((responseJson) => {
      if (responseJson.token != undefined)
      {
        // set the token in local storage, save 
        //    username and password for renewal if we need it
        AsyncStorage.setItem('@Gradery:token', responseJson.token);
        AsyncStorage.setItem('@Gradery:email', this.state.email);
        AsyncStorage.setItem('@Gradery:password', this.state.password);
        // redirect to the Tracker page
        this.props.navigator.replace({name: "Tracker"})
        return responseJson.token;
      }
      else
        this.setState({error: "Invalid Email or Password"});
    })
    .catch((error) => {
      console.error(error);
    });
  },

  componentDidMount() {
    this._loadInitialState().done();
  },
  
  async _loadInitialState() {
    try {
      var token = await AsyncStorage.getItem('@Gradery:token');
      if (token !== null) //already logged in
        this.props.navigator.replace({name: "Tracker"})
      else
        this.setState({token: null});
    }
    catch(error){
      console.error(error);
    }
  },

  getInitialState() {
    return {
      token: null,
      error: '',
      selectedMemberText: '',
      selectedMemberId: 0
    };
  },

  render(){
    return(
      <View style={styles.loginContainer}>
        <Text style={styles.title}>Attendance Tracker</Text>
        <Text style={styles.error}>{this.state.error}</Text>
        <TextInput 
          style={styles.textInput}
          placeholder="Email"
          onChangeText={(text) => this.setState({email: text})}
        />
        <TextInput 
          style={styles.textInput}
          placeholder="Password"
          onChangeText={(text) => this.setState({password: text})}
        />
        <TouchableHighlight
          style={styles.button}
          onPress={this.authenticate}>
          <View>
            <Text>login</Text>
          </View>
        </TouchableHighlight>
      </View>
    )
  }
});

const styles = StyleSheet.create({
  loginContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    paddingLeft: 10,
    paddingRight: 10,
  },
  trackerContainer:{
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    paddingLeft: 10,
    paddingRight: 10,
  },
  title:{
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 50
  },
  error: {
    height: 15,
    color: 'red',
  },
  button:{
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    marginTop: 5,
    marginBottom: 5,
    paddingRight: 20,
    paddingLeft: 20,
    paddingTop: 15,
    paddingBottom: 15,
    backgroundColor: '#33AAFF',
  },
  scanButton: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    height: 40,
    backgroundColor: '#33AAFF',
    paddingRight: 5,
    paddingLeft: 5,
  },
  tardyButton:{
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    height: 80,
    backgroundColor: 'red',
    paddingRight: 5,
    paddingLeft: 5,
    marginTop: 100
  },
  textInput: {
    height: 40,
    borderStyle: 'solid',
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 5,
    marginTop: 5,
    marginBottom: 5,
    paddingRight: 10,
    paddingLeft: 10,
  },
  textInputWithButton: {
    height: 40,
    borderStyle: 'solid',
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 5,
    marginTop: 5,
    marginBottom: 5,
    marginRight: 10,
    paddingRight: 10,
    paddingLeft: 10,
    width: 250
  }
});