import React, { Component } from 'react';
import {
  AppRegistry,
  Dimensions,
  StyleSheet,
  Text,
  TouchableHighlight,
  View
} from 'react-native';
import Camera from 'react-native-camera';

export default Scanner = React.createClass({
	cancel(){
		this.props.navigator.replace({name: "Tracker"})
	},
	handleScan(data, bounds){
		console.log(data);
		this.props.navigator.replace({name: "Tracker", badge_id: data})
	},
	render() {
	    return (
	      <View style={styles.container}>
	        <Camera
	          style={styles.preview}
	          aspect={Camera.constants.Aspect.fill}
	          type={Camera.constants.Type.back}
	          defaultTouchToFocus
	          captureAudio={false}
	          captureMode={Camera.constants.CaptureTarget.temp}
	          onBarCodeRead={this.handleScan}>
	          <View style={styles.overlay}>
	          	<TouchableHighlight
	            style={styles.scanButton}
	            onPress={this.cancel}>
	              <View>
	                <Text>Cancel</Text>
	              </View>
	            </TouchableHighlight>
	          </View>
	        </Camera>
	      </View>
	    );
	}
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width
  },
  overlay:{
  	bottom: 0,
  }
});