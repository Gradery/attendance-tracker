# == Schema Information
#
# Table name: members
#
#  id         :integer          not null, primary key
#  school_id  :integer
#  house_id   :integer
#  badge_id   :string
#  email      :string
#  name       :string
#  created_at :datetime
#  updated_at :datetime
#  deleted_at :datetime
#  grade      :string
#

class Member < ActiveRecord::Base
	acts_as_paranoid
	
	belongs_to :school
	belongs_to :house
	has_many :point_assignments
	has_many :attendance_events

	validates :badge_id, presence: true
end
