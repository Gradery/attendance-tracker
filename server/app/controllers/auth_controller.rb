class AuthController < ApplicationController
	skip_before_action :authenticate, only: [:login]
	def login
		ap params
		if Staff.where(:email => params['email'].downcase).exists?
			staff = Staff.find_by_email(params['email'].downcase)
			if staff.valid_password?(params['password'])
				token = JwtHelper.encode('staff_id' => staff.id)
				render json: {:token => token}
			else
				render json: {success: false}, status: 401
			end
		else
			render json: {success: false}, status: 401
		end
	end

	def logout

	end
end
