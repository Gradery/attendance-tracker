class StaffController < ApplicationController
	def get
		render json: {staff: @current_staff}
	end

	def update
		render json:{success: true}
	end
end
