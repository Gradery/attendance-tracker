class MembersController < ApplicationController

	def list
		members = Member.where(:school_id => @current_staff.school_id).to_a
		render json:{members: members}
	end

	def search
		if !params['badge_id'].nil?
			if Member.where(:badge_id => params['badge_id'], :school_id => @current_staff.school_id).exists?
				member = Member.where(:badge_id => params['badge_id'], :school_id => @current_staff.school_id).first
				render json: {member: member}
			else
				render json:{errors:["Member Not Found"]}, status: 404
			end
		else
			render json: {errors: ["Missing Parameters"]}, status: 422
		end
	end

	def get_one
		if Member.where(:id => params['id'], :school_id => @current_staff.school_id).exists?
			member = Member.find(params['id'])
			render json: {member: member}
		else
			render json:{errors:["Member Not Found"]}, status: 404
		end
	end

	def mark_tardy
		if Member.where(:id => params['id']).exists?
			member = Member.find(params['id'])
			if member.school_id == @current_staff.id
				AttendanceEvent.create!(member: member, event_type: "tardy")
				render json: {success: true}
			else
				render json: {success: false, error: "Unauthorized"}, status: 401
			end
		else
			render json: {success: false, error: "Member not found"}, status: 404
		end
	end
end
