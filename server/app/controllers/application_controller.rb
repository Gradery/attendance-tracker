class ApplicationController < ActionController::API
	before_action :cors_preflight_check
  before_action :cors_set_access_control_headers
  before_action :authenticate

	private

	def authenticate
		if claims and staff = Staff.find_by(id: claims['staff_id'])
    	@current_staff = staff
  	else
    	return render_unauthorized errors: { unauthorized: ["You are not authorized perform this action."] }
  	end
	end

	def claims
    begin
      auth_header = request.headers['Authorization']
      if !auth_header.nil?
        token = auth_header.split(' ').last
      else
        # check params
        token = params['token']
      end
      JwtHelper.decode(token)
    rescue
      logger.debug "rescue happened?"
      nil
    end
  end

  def render_unauthorized(payload)
    render json: payload.merge(response: { code: 401 })
  end

  def cors_set_access_control_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'GET, OPTIONS'
    headers['Access-Control-Max-Age'] = "1728000"
  end

  def cors_preflight_check
    if request.method == :options
      headers['Access-Control-Allow-Origin'] = '*'
      headers['Access-Control-Allow-Methods'] = 'GET, OPTIONS'
      headers['Access-Control-Allow-Headers'] = 'X-Requested-With, X-Prototype-Version'
      headers['Access-Control-Max-Age'] = '1728000'
      render :text => '', :content_type => 'text/plain'
    end
  end
end
