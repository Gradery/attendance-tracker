# == Schema Information
#
# Table name: attendance_events
#
#  id         :integer          not null, primary key
#  member_id  :string
#  event_type :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :attendance_event do
    member_id "MyString"
    event_type "MyString"
  end
end
