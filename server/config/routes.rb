Rails.application.routes.draw do
  scope '/auth' do
  	post 'login' => 'auth#login'
  	post 'logout' => 'auth#logout'
  end
  get '/staff' => 'staff#get'
  post '/staff' => 'staff#update'
  scope '/members' do
  	get 'search' => 'members#search'
  	get ':id' => 'members#get_one'
  	post ':id/tardy' => 'members#mark_tardy'
  end
  get '/members' => 'members#list'

  root 'auth#login'
end
