ActiveAdmin.register Member do

  permit_params :school_id, :house_id, :email, :name, :badge_id, :grade

  index do
    selectable_column
    column :name
  	column :badge_id
    column "Events" do |m|
      link_to m.attendance_events.count, admin_member_events_path(m)
    end
    actions
  end

  filter :name
  filter :badge_id
  filter :school, :collection => proc { School.all }, if: proc{ current_admin_user.school_id.nil?}

  form do |f|
    f.inputs "Member" do
      f.input :name
      f.input :badge_id
    end
    f.actions
  end

controller do
    def scoped_collection
      if !current_admin_user.school_id.nil?
        Member.where(:school_id => current_admin_user.school_id).all
      else
        Member.all
      end
    end

    def update
	    if !current_admin_user.school_id.nil?
        params[:member][:school_id] = current_admin_user.school_id
      end
	    super
	  end

	  def create
	    if !current_admin_user.school_id.nil?
        params[:member][:school_id] = current_admin_user.school_id
      end
	    super
	  end
  end

csv do
  column :badge_id
  column :name
  column "Events" do |member|
    member.events.count
  end
end

end
