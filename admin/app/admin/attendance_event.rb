ActiveAdmin.register AttendanceEvent, as: "Event" do
  permit_params :member_id, :event_type

  belongs_to :member

  index do
    selectable_column
    column :event_type
    column :created_at
    actions
  end

end