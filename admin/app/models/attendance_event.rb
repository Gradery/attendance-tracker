# == Schema Information
#
# Table name: attendance_events
#
#  id         :integer          not null, primary key
#  member_id  :string
#  event_type :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class AttendanceEvent < ApplicationRecord
	belongs_to :member
end
