# == Schema Information
#
# Table name: staffs
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  created_at             :datetime
#  updated_at             :datetime
#  school_id              :string
#  house_id               :integer
#  grade                  :string
#  deleted_at             :datetime
#  name                   :string
#

class Staff < ActiveRecord::Base
  acts_as_paranoid
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :email, :school_id, presence: true
  validates :password, :password_confirmation, presence: true, on: :create
  validates :password, confirmation: true

  belongs_to :school
  #belongs_to :house

  #has_many :point_assignments

  def to_s
  	email
  end
end
