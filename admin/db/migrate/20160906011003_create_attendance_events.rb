class CreateAttendanceEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :attendance_events do |t|
      t.string :member_id
      t.string :event_type

      t.timestamps
    end
  end
end
