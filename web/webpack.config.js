module.exports = {
	entry: "./js/app.jsx",
	output: {
		filename: "./js/bundle.js"
	},
	debug: true,
	devtool: 'source-map',
	module: {
		loaders: [
		{
			test: /\.js/,
			exclude: /(node_modules|bower_components)/,
	      loader: 'babel', // 'babel-loader' is also a legal name to reference
	      query: {
	      	presets: ['es2015', 'react']
	      }
	  },
	  {
	  	test: /\.css/,
	  	loader: "style-loader!css-loader"
	  }
	  ]
	},
	externals: {
		'Config': JSON.stringify(process.env.ENV === 'production' ? {
			serverUrl: "http://tracker.gradery.com/api"
		} : {
			serverUrl: "http://localhost:3000"
		})
	}
}