import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {Router, Route, Link, IndexRoute, browserHistory} from 'react-router';

import Tracker from './components/tracker.jsx';
import Login from './components/login.jsx';

var App = React.createClass({
  logout(){
    localStorage.setItem('token',null);
    browserHistory.push('/login');
  },

  render() {
    return (
      <div>
        <nav className="navbar navbar-default navbar-fixed-top">
          <div className="container-fluid">
            <div className="navbar-header">
              <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span className="sr-only">Toggle navigation</span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
              </button>
              <a className="navbar-brand" href="#">Gradery Tracker</a>
            </div>
            <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul className="nav navbar-nav navbar-right">
                <li><a href="https://gradery.freshdesk.com/helpdesk">Support</a></li>
                <li><a href="#" onClick={this.logout}>Logout</a></li>
              </ul>
            </div>
          </div>
        </nav>
        <div className='row' style={{paddingTop: 80}}>
          {this.props.children}
        </div>
      </div>
    )
  }
})

ReactDOM.render((
  <Router history={browserHistory} >
    <Route path="/" component={App}>
      <IndexRoute component={Tracker} />
      <Route path="login" name='login' component={Login} />
    </Route>
  </Router>
), document.getElementById("body"));


// var AttendanceTracker = React.createClass({

//   render() {
//     try {
//       var token = localStorage.getItem('token').done();
//       var initialPage = "";
//       if (token !== null) //already logged in
//         initialPage = "Tracker"
//       else
//         initialPage = "Login"
//     }
//     catch(error){
//       console.error(error);
//     }
//     return(
//       Hi
//     );
//   }
// });