import React, { Component } from 'react';

import { browserHistory } from 'react-router';

import Config from 'Config'

var Login = React.createClass({
  authenticate() {
    var self = this;
    console.log(this.state);
    $.post(Config.serverUrl+'/auth/login', {
      email: self.state.email,
      password: self.state.password
    })
    .success(function(data){
      if (data.token != undefined)
      {
        // set the token in local storage, save 
        localStorage.setItem('token', data.token);
        // // redirect to the Tracker page
        setTimeout(function(){
          browserHistory.push('/');
        }, 1000);
      }
      else
        self.setState({error: "Invalid Email or Password"});
    })
    .error(function(error) {
      console.error(error);
      self.setState({error: "Invalid Email or Password"});
    });
  },

  getInitialState() {
    return {
      token: null,
      error: '',
      selectedMemberText: '',
      selectedMemberId: 0,
      email: '',
      password: ''
    };
  },

  setEmail(event){
    this.setState({email: event.target.value});
  },

  setPassword(event){
    this.setState({password: event.target.value});
  },

  render(){
    return(
      <div className="col-xs-12">
        <center>
          <h2>Gradery Tracker</h2>
          <h4 className="error">{this.state.error}</h4>
          <div className="col-md-6 col-md-offset-3">
            <div className="form-group">
              <input
                type='text'
                className='form-control'
                placeholder="Email"
                onChange={this.setEmail}
                />
            </div>
            <div className="form-group">
              <input
                type='password'
                placeholder="Password"
                className='form-control'
                onChange={this.setPassword}
               />
            </div>
            <button
              className='btn btn-primary'
              onClick={this.authenticate}
              >
              Login
            </button>
          </div>
        </center>
      </div>
      // <View style={styles.loginContainer}>
      //   <Text style={styles.title}>Attendance Tracker</Text>
      //   <Text style={styles.error}>{this.state.error}</Text>
      //   <TextInput 
      //     style={styles.textInput}
      //     placeholder="Email"
      //     onChangeText={(text) => this.setState({email: text})}
      //   />
      //   <TextInput 
      //     style={styles.textInput}
      //     placeholder="Password"
      //     onChangeText={(text) => this.setState({password: text})}
      //   />
      //   <TouchableHighlight
      //     style={styles.button}
      //     onPress={this.authenticate}>
      //     <View>
      //       <Text>login</Text>
      //     </View>
      //   </TouchableHighlight>
      // </View>
    );
  }
});

export default Login; 