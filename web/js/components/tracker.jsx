import React, { Component } from 'react';

import { browserHistory } from 'react-router';

import Autosuggest from 'react-autosuggest';

import Config from 'Config'

var Tracker = React.createClass ({
	componentDidMount() {
	    this._loadInitialState();
	},
	  
	_loadInitialState() {
	    try {
	      var token = localStorage.getItem('token');
	      if (token !== null){
	      	var self = this;
	      	this.setState({token: token});
	      	setTimeout(function(){
	      		self.getStudents();
	      	}, 1000);
	      }
	      else // redirect to login page
			browserHistory.push('/login');
	    }
	    catch(error){
	      console.error(error);
	    }
	},

	getStudents(){
		var self = this;
		$.get(Config.serverUrl+"/members?token="+this.state.token)
		.success(function(data){
			if (data.errors === undefined)
			{
				self.setState({members: data.members});
			}
			else{
				if (data.response.code === 401){
					console.log("Need to log back in");
					self.setState({token: null});
					browserHistory.push('/login');
				}
			}
		})
		.error(function(error){
			console.log(error);
		})
	},

	onChange(event, { newValue }){
	    this.setState({
	      	selectedMemberText: newValue
	    });
	},

	getSuggestions(value) {
	  	var inputValue = value.value.trim().toLowerCase();
	  	var inputLength = inputValue.length;
	  	var self = this;

	  	if (inputLength === 0 || this.state.members === undefined)
	  		return [];
	  	else {
	  		return this.state.members.filter(function(member){
		  		if (member.name.toLowerCase().slice(0, inputLength) === inputValue)
		    		return true;
		    	else if (member.badge_id.toLowerCase().slice(0, inputLength) === inputValue)
		    		return true;
		    	else
		    		return false;
		  	});
	  	}
	},

	onSuggestionsFetchRequested(value){
	    this.setState({
	      	suggestions: this.getSuggestions(value)
	    });
	},
 
	onSuggestionsClearRequested(){
	    this.setState({
	      	suggestions: []
	    });
	},

	onSuggestionSelected(event, suggestion){
		this.setState({
			selectedMemberId: suggestion.suggestion.id,
			selectedMemberObj: suggestion.suggestion
		});
	},

	renderSuggestion(suggestion) {
	  return (
	    <span>{suggestion.name} ({suggestion.badge_id})</span>
	  );
	},

	getSuggestionValue(suggestion) { 
  		return suggestion.name + " (" + suggestion.badge_id + ")";
	},

  	markTardy(){
  		var self = this;
  		$.post(Config.serverUrl+"/members/"+this.state.selectedMemberId+"/tardy",{
  			token: this.state.token
  		})
  		.success(function(data){
  			if (data.success !== undefined && data.success)
  			{
  				swal({
  					title: "Success",
  					text: self.state.selectedMemberObj.name + " marked tardy",
  					type: "success",
  					timer: 2000
  				});
  				self.clearSearch();
  			}
  		})
  		.error(function(error){
  			console.log(error);
  		});
  	},

	getInitialState() {
	    return {
	      token: null,
	      error: '',
	      selectedMemberText: '',
	      selectedMemberId: 0,
	      badge_id: 0,
	      loading: false,
	      value: '',
      	  suggestions: []
	    };
	},

	clearSearch(){
		this.setState({
			selectedMemberText: "",
			selectedMemberId: 0,
			selectedMemberObj: undefined
		});
	},

	getMemberDetails(){
		console.log(this.state.selectedMemberObj)
		if (this.state.selectedMemberObj === undefined)
			return(<span></span>);
		else{
			return(
				<div className="row">
	    			<div className="col-xs-6">
	    				<center>
		    				<button className='btn btn-danger' onClick={this.markTardy}>
		    					Mark Tardy
		    				</button>
		    			</center>
	    			</div>
	    			<div className='col-xs-6'>
	    				<center>
		    				<button className='btn btn-primary' onClick={this.clearSearch}>
		    					Clear
		    				</button>
		    			</center>
	    			</div>
	    		</div>
			);
		}
	},

	render(){
		var value = this.state.selectedMemberText;
		var selectedMemberObj = this.state.selectedMemberObj;
		var suggestions = this.state.suggestions;
		var member_details = this.getMemberDetails();
	    const inputProps = {
	      placeholder: 'Name / Badge ID',
	      value,
	      onChange: this.onChange,
	      className: "form-control"
	    };
		return(
			<div className="row">
				<div className='col-xs-12 col-md-6' style={{paddingTop: 10}}>
					<Autosuggest
        				suggestions={suggestions}
        				onSuggestionsUpdateRequested={this.onSuggestionsUpdateRequested}
        				onSuggestionsClearRequested={this.onSuggestionsClearRequested}
        				getSuggestionValue={this.getSuggestionValue}
        				onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
        				onSuggestionSelected={this.onSuggestionSelected}
        				renderSuggestion={this.renderSuggestion}
        				inputProps={inputProps}
        			/>
				</div>
				<div className='col-xs-12 col-md-6' style={{paddingTop: 10}}>
					{member_details}
				</div>
			</div>
		// 	<View style={styles.loginContainer}>
		// 	  <ActivityIndicator
  //       		animating={this.state.loading}
  //       		size="large"
  //     		  />
		// 	  <Text style={styles.error}>{this.state.error}</Text>
	 //          <View style={styles.trackerContainer}>
	 //            <TextInput 
	 //              placeholder="Student Name or Badge ID"
	 //              style={styles.textInputWithButton}
	 //              autoCorrect={true}
	 //              autoFocus={true}
	 //              value={this.state.selectedMemberText}
	 //              onChangeText={(text) => this.setState({selectedMemberText: text})}
	 //            />
	 //            <TouchableHighlight
	 //            style={styles.scanButton}
	 //            onPress={this.scan}>
	 //              <View>
	 //                <Text>Scan</Text>
	 //              </View>
	 //            </TouchableHighlight>
	 //          </View>
	 //          {(() =>{
	 //          	if (this.state.selectedMember !== undefined){
	 //          		return(
	 //          			<View>
		// 		          	<Text>
		// 		            	Name: {this.state.selectedMember.name}
		// 		            </Text>
		// 		            <Text>
		// 		            	Grade: {this.state.selectedMember.grade}
		// 		            </Text>
		// 		            <TouchableHighlight
		// 		            style={styles.tardyButton}
		// 		            onPress={this.markTardy}>
		// 		              <View>
		// 		                <Text style={{color: "white"}}>Mark Tardy</Text>
		// 		              </View>
		// 		            </TouchableHighlight>
		// 		        </View>
	 //          		)
	 //          	}
	 //          	else {
	 //          		return(
	 //          			<View style={{height: 245}}/>
	 //          		)
	 //          	}
	 //          })()}
	 //        </View>
		);
	}
});

export default Tracker;