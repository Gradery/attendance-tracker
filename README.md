# Gradery Attendence Tracker

This application support tracking attendance across a school. Currently, the app is only concerned with recording and tracking tardy cases (late to school or late to class), but will expand to include managing absences and notifying the school about sick students.

## Dependencies
- Go 1.5+
- React
- React-Native
